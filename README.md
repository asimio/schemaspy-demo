# README #

Accompanying source code for blog entry at https://tech.asimio.net/2020/11/27/Documenting-your-relational-database-using-SchemaSpy.html


### Requirements ###

* Java 5+
* Maven 2.2.1+

### Building the database documentation ###

```
mvn schemaspy:schemaspy
```

### Report

```
./target/site/schemaspy/index.html
```

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero